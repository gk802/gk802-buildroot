#############################################################
#
# gk802-fwu 
#
#############################################################

GK802_FWU_VERSION = master
GK802_FWU_SITE = https://bitbucket.org/gk802/gk802-fwu.git
GK802_FWU_SITE_METHOD = git

define GK802_FWU_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0700 $(@D)/target/bin/gk802_fwu $(TARGET_DIR)/bin
endef

#TODO: how to generate update file? 

$(eval $(generic-package))
