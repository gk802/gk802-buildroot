#############################################################
#
# libass
#
#############################################################

LIBASS_VERSION = 0.10.1
LIBASS_SOURCE = libass-$(LIBASS_VERSION).tar.xz
LIBASS_SITE = http://libass.googlecode.com/files
LIBASS_LICENSE =  New BSD License
LIBASS_LICENSE_FILES = COPYING
LIBASS_INSTALL_STAGING = YES

LIBASS_CONF_OPT = \
    --disable-harfbuzz

LIBASS_DEPENDENCIES = fontconfig freetype 

$(eval $(autotools-package))
