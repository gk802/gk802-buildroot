#############################################################
#
# imx-lib
#
#############################################################

IMX_LIB_GLES_VERSION = master
# No official download site from freescale, just this mirror
IMX_LIB_GLES_SITE = https://bitbucket.org/gk802/gpu-viv-bin-mx6q-3.0.35-4.0.0.git
IMX_LIB_GLES_SITE_METHOD = git
IMX_LIB_GLES_LICENSE = LGPLv2.1+
# No license file included

IMX_LIB_GLES_INSTALL_STAGING = YES

define IMX_LIB_GLES_BUILD_CMDS
endef

define IMX_LIB_GLES_INSTALL_STAGING_CMDS
	$(INSTALL) -D -m 0755 $(@D)/usr/lib/*.so* $(STAGING_DIR)/usr/lib
	$(INSTALL) -D -m 0755 -t $(@D)/usr/lib/pkgconfig $(STAGING_DIR)/usr/lib/pkgconfig
	$(INSTALL) -D -m 0755 -t $(@D)/usr/lib/directfb-1.4-0 $(STAGING_DIR)/usr/lib/directfb-1.4-0
	$(INSTALL) -D -m 0755 -t $(@D)/usr/lib/dri $(STAGING_DIR)/usr/lib/dri
	$(INSTALL) -D -m 0755 -t $(@D)/usr/include $(STAGING_DIR)/usr/include
endef

define IMX_LIB_GLES_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/usr/lib/*.so* $(TARGET_DIR)/usr/lib
	$(INSTALL) -D -m 0755 -t $(@D)/usr/lib/directfb-1.4-0 $(STAGING_DIR)/usr/lib/directfb-1.4-0
	$(INSTALL) -D -m 0755 -t $(@D)/usr/lib/dri $(STAGING_DIR)/usr/lib/dri
endef

$(eval $(generic-package))
