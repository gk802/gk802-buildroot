#!/bin/bash 

DEVICE=/dev/sdb
echo -n "SDcard device ($DEVICE): "
read INPUTDEV
if [ -z $INPUTDEV ]; then
    echo empty
else
    echo $INPUTDEV
fi

umount ${DEVICE}?

# wipe mbr
dd if=/dev/zero of=${DEVICE} conv=fsync bs=512 count=2047

# format card
parted $DEVICE mklabel msdos

# partition
sfdisk --force $DEVICE << EOF
unit: sectors
${DEVICE}1 : start=    40960, size=   131072, Id=83
${DEVICE}2 : start=   172032, size= 15106048, Id=83
${DEVICE}3 : start=        0, size=        0, Id= 0
${DEVICE}4 : start=        0, size=        0, Id= 0
EOF

# bootloader
dd if=output/images/u-boot.imx of=${DEVICE} conv=fsync bs=1K seek=1

# boot partition
mkfs.ext2 ${DEVICE}1
mount -t ext2 ${DEVICE}1 /mnt
rm -fr /mnt/*
mkdir -p /mnt/boot
cp output/images/uImage /mnt/boot
umount /mnt

mkfs.ext4 ${DEVICE}2
mount -t ext4 ${DEVICE}2 /mnt
rm -fr /mnt/*
tar xvf output/images/rootfs.tar -C /mnt
umount /mnt

sync
umount ${DEVICE}?
